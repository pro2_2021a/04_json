package experiments;

import java.io.IOException;
import com.google.gson.*;
import org.json.*;
import experiments.dataModel.Rozvrh;

public class StagClient
{
    public static void main(String[] args) throws IOException {
        String json = Utils.GetContentFromURL("https://stag-demo.uhk.cz/ws/services/rest2/rozvrhy/getRozvrhByMistnost?semestr=ZS&budova=J&mistnost=J1&outputFormat=json");
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Rozvrh rozvrh = gson.fromJson(json, Rozvrh.class);
        String newJson = gson.toJson(rozvrh);

        JSONObject jsonObject = new JSONObject(json);
    }
}
