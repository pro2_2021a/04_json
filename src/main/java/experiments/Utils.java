package experiments;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Utils
{
    public static int GetResponseCodeFromURL(String url) throws IOException
    {
        URL url1 = new URL(url);//your url i.e fetch data from .
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        return conn.getResponseCode();
    }

    public static String GetContentFromURL(String url) throws IOException
    {
        URL url1 = new URL(url);//your url i.e fetch data from .
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP Error code : "
                    + conn.getResponseCode());
        }
        java.util.Scanner s = new java.util.Scanner(conn.getInputStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
